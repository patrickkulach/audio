#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>


void main() {
    int bufsize = 16;
    for (u_int16_t index = 0; index < bufsize; index++) {
        int reverseindex = 0;
        for (int i = 0; i < 16; i++) {
            int bit = ((1 << i) & index) >> i;
            reverseindex |= bit << (15 - i);
        }
        printf("%X\n", reverseindex);
    }
}
