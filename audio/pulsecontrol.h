#include <pulse/simple.h>
#include <stdio.h>

//This file and it's .c file should provide all interfaces with the pulseaudio API.

#ifndef PULSE_CONTROL_H
#define PULSE_CONTROL_H

void paSimpleRecordOpen(pa_simple** s, pa_sample_spec * ss, char* streamName);

void receiveInput(int16_t *buffer, int bufsize, pa_simple *s);

void paSimplePlayer(pa_simple** s, char* streamName);

void sendOutput(int16_t *buffer, int bufsize, pa_simple *s);

#endif