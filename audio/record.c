#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <pulse/pulseaudio.h>
#include <pulse/simple.h>
#include <curses.h>


#define SAMPLE_RATE 44100 //Hz
#define LENGTH 50 //seconds
#define PI 3.141592654359

int main() {
    initscr();
    noecho();
    int h, w;
    WINDOW *win = newwin(LINES, COLS, 0, 0);
#if 0
while(1) {
    for (int x = 0; x < COLS; x++) {
        for (int y = 0; y < LINES; y++) {
            wmove(win, y, x);
            waddch(win, 'a' + x % 25);
        }
    }
    wrefresh(win);
}
#endif

#if 1
    //FILE *outf = fopen("rawaudio.raw", "w");
    //FILE *outf = fopen(stdout, "w");
    FILE *outf = stdout;
    //fputc('a', outf);
    
    //Ukryte z strona pulseaudio
    //https://freedesktop.org/software/pulseaudio/doxygen/simple.html
    pa_simple *s;
    pa_sample_spec ss;
    ss.format = PA_SAMPLE_S16NE;
    ss.channels = 2;
    ss.rate = 44100;
    int error;
    s = pa_simple_new(NULL,               // Use the default server.
                  "Audio Stream",           // Our application's name.
                  PA_STREAM_RECORD,
                  NULL,               // Use the default device.
                  "TestStream",            // Description of our stream.
                  &ss,                // Our sample format.
                  NULL,               // Use default channel map
                  NULL,               // Use default buffering attributes.
                  &error               // Ignore error code.
                  );
    if (error != 0) {
        printf("Couldn't open audio server with error code %d\n", error);
    }

    int per = SAMPLE_RATE * 2 * 4;
    int beats = 10;
    for (int i = 0; i < beats; i++) {
        char data[per];
        pa_simple_read(s, data, per, &error); //dwa razy poniewasz mamy 16 bytów

        if (error < 0) {
            printf("Couldn't read audio input with error code %d\n", error);
        }
        for (int j = 0; j < per; j++) {
            putc(data[j], outf);
        }
    }
    /*
    pa_simple_write(s, data, SAMPLE_RATE / 100, &error);
    if (error < 0) {
        printf("Couldn't play back with error code %d\n", error);
    }*/
/*
    for (int i = 0; i < SAMPLE_RATE * LENGTH; i++) {
        double t = (double) i / (double) SAMPLE_RATE;
        double sampfloat = sin(500 * t);
        int samp = (int) (-(double) (0x8000 - 1) * sampfloat);
        char front = samp >> 8;
        char end = samp;
        fputc(front, outf);
        fputc(end, outf);
    }*/
    fclose(outf);

    pa_simple_free(s);
#endif
}
