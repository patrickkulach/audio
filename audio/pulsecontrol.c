#include "pulsecontrol.h"


/*
 * Wrapper function to set up a simple Pulse Audio recorder with default values.
 * These values are set to use the default server, default device, and default channels.
 * This recording sets up a 44100 sample / second recording with 1 channel, at 16 bits of depth.
 * The function takes a name of the stream, and a pa_simple** which will be modiified in the caller's
 * scope.
 */
void paSimpleRecordOpen(pa_simple** s, pa_sample_spec *ss, char* streamName) {
    int error;
    //pa_sample_spec ss;
    ss->format = PA_SAMPLE_S16NE;
    ss->channels = 1;
    ss->rate = 44100;
    *s = pa_simple_new(NULL,           // Use the default server.
              "Audio Stream",     // Our application's name.                  
              PA_STREAM_RECORD,
              NULL,               // Use the default device.
              streamName,       // Description of our stream.
              ss,                // Our sample format.
              NULL,               // Use default channel map
              NULL,               // Use default buffering attributes.
              &error             // Ignore error code.
              );
    //printf("%d Error\n", error);

}

/*
 * Fills in the array buffer with stream recording data.
 */ 
void receiveInput(int16_t *buffer, int bufsize, pa_simple *s) {
    int error = 0;
    pa_simple_read(s, buffer, bufsize, &error);
    if (error < 0) printf("Error reading stream data\n");
}

void paSimplePlayer(pa_simple** s, char* streamName) {
    int error;
    pa_sample_spec ss;
    ss.format = PA_SAMPLE_S16NE;
    ss.channels = 1;
    ss.rate = 44100;
    *s = pa_simple_new(NULL,           // Use the default server.
              "Audio Stream",     // Our application's name.                  
              PA_STREAM_PLAYBACK,
              NULL,               // Use the default device.
              streamName,       // Description of our stream.
              &ss,                // Our sample format.
              NULL,               // Use default channel map
              NULL,               // Use default buffering attributes.
              &error              // Ignore error code.
              );
}

void sendOutput(int16_t *buffer, int bufsize, pa_simple *s) {
    int error;
    pa_simple_write(s, buffer, bufsize, &error);
}