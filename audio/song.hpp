#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>

extern "C" {
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswresample/swresample.h>
}
#include "pulsecontrol.h"


#ifndef SONG_H
#define SONG_H

typedef enum {
    S16LE, 
    S16BE,
    F32LE,
    F32BE
} audioformat;

class Song {
    public:
    Song(char* location);
    Song();
    ~Song();

    void loadFile(char* location);

    int byteLength; // Size in number of u_int_16s
    int timeLength; // Time in ms
    void * data;
    int rate; // Sample rate in Hz
    audioformat format;
};

#endif