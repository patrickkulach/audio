#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <pulse/simple.h>
#include <curses.h>
#include <fcntl.h>
#include <string.h>
#include <libavcodec/mediacodec.h>


#include "fft.h"
#include "pulsecontrol.h"

#define PI 3.141592654359

#define SAMPLE_RATE 44100 //Hz
#define LENGTH 50 //seconds
#define PROC_TIME 1.0f // Time in seconds that it analyses the frequency for
#define EXP_CONST 3.0f // The inputs are multiplied by exp(EXP_CONST * t) when put through the laplace transform
#define MIN_FREQ 100 //Hz
#define MAX_FREQ 2000 //Hz
#define FREQ_SAMP COLS //10 //Amount of samples displayed
#define INTER_SAMP 1 //Amount of samples taken to be averaged out for the displayed sample
#define DEFAULT_COLOR 2

// void receiveInput(int16_t *buffer, int bufsize, pa_simple *s) {
//     int error;
//     pa_simple_read(s, buffer, bufsize, &error);
//     if (error < 0) printf("Error reading stream data\n");
// }

void processInput(int16_t *buffer, int bufsize, WINDOW* win, long *re, long *im) {
    double dt = 1 / (double) 44100;
    int sum = 0; //All we need is an int, since the maximum will be 2^16 anyway, so we can add 2^16 times of those!
    for (int j = 0; j < FREQ_SAMP * INTER_SAMP; j++) {
        re[j] = re[j] * .75;
        im[j] = im[j] * .75;
    }
    for (int j = 0; j < FREQ_SAMP * INTER_SAMP; j++) {
        for (int i = 0; i < bufsize; i++) {
            double t = -i * dt;
            double w = (MAX_FREQ - MIN_FREQ) / FREQ_SAMP / INTER_SAMP * 2 * PI * j + MIN_FREQ * 2 * PI; //Omega in rad/s
            re[j] += buffer[i] * cos(w * t);
            im[j] += buffer[i] * sin(w * t);
            //printf("freq: %f\n", w);
        }
    }
    //printf("%d \n", sum / bufsize);
    for (int x = 0; x < FREQ_SAMP; x++) {
        int amp = 0;
        for (int j = x * INTER_SAMP; j < x * INTER_SAMP + INTER_SAMP; j++) {
            long a = re[j];
            long b = im[j];
            amp += (int) sqrt(a * a + b * b);
        }
        amp /= INTER_SAMP;
        for (int y = 0; y < LINES; y++) {
            wmove(win, y, x);
            int level = (int) (log(amp) * 4 - 30);
            //if (level < 10) level = 1;
            if (LINES - y == level) {
                waddch(win, 'X');
            } else {
                waddch(win, ' ');
            }
        }
    }
    wrefresh(win);
    //printf("\n");

    //usleep(100); //Sleep for 100ms - Arbitrary value
    
}


void processInputfft(int16_t *buffer, int bufsize, WINDOW* win, int *previous) {
    //Extra step, convert int16_t to int, should be reorganized if it works
    int data[bufsize];
    double freq[bufsize];
    for (int i = 0; i < bufsize; i++) data[i] = (int) buffer[i];
    fft(data, freq, bufsize);
    
    int aliassamplemult = 10; 
    for (int x = 0; x < COLS; x++) {
        // double level = data[bufsize / COLS * x / 2 + aliassamples];
        // We can divide by many different integers to get different distrubutions, for this, we cap at 21500 / 8 Hz, to cap at 21500 you change the 8 into a 2
        //int aliassamples = aliassamplemult * 
        int harmonic = bufsize / 24 * ((exp((double) x / (double) COLS) - 1) / (2.71828 - 1)); //Replace 24 with 16 for the one you're used to
        //ewentualny poprawa: mogęmy położyć int harmonic w arraju, bo są konstantów jak długość sie nie zmienia
        //int aliassamples = aliassamplemult * harmonic / 1000;
        //int aliassamples = 0;
        //harmonic += aliassamples;
        double level = 0;
        //if (aliassamples == 0) level += freq[harmonic] * harmonic;
        // for (int a = -aliassamples; a < aliassamples; a++) {
        //     //level += freq[bufsize / COLS * x / 2 + a] * (bufsize / COLS * x / 2 + a);
        //     level += freq[harmonic + a] * (harmonic + a);
        // }
        level = freq[harmonic] / (double) (100);
        if (level > .5) {
            level = log(level) * 2 - 20;
            level = pow(level, 1.1) - 10;
        } else level = 1;
        //level = .2 * previous[x] + level * .8;
        if (isnan(level)) level = 1;
        if (level < previous[x]) {
            level = previous[x] - ((double) previous[x] - level) * .05;
        }
        if (level <= 1) level = 1.1;
        //previous[x] = level;
        int l = floor(level);
        //printf("%f ", level);
        #if 0
        for (int y = 0; y < LINES; y++) {
            wmove(win, y, x);
            if (l <= 1) l = 1;
            if (LINES - y <= l) {
                waddch(win, '#');
            } else {
                waddch(win, ' ');
            }

        }
        #endif
        //Begin the drawings
        if (level > previous[x]) for (int y = floor(previous[x]); y < floor(level); y++) {
            wmove(win, LINES - y - 1, x);
            waddch(win, '#'); 
        }
        else for (int y = floor(previous[x]); y >= floor(level); y--) {
            wmove(win, LINES - y - 1, x);
            waddch(win, ' ');
        }
        previous[x] = level;

    }
    wrefresh(win);

}

void sendOutputArduino(int fd, FILE* log, int *data) {
    double total = 0;
    for (int i = 0; i < FREQ_SAMP; i++) {
        total += data[i];
    }
    total /= (double) FREQ_SAMP;
    unsigned int send = total * total * 128;
    
    printf("%u\n", send);
    write(fd, &send, sizeof(int));
    //read(fd, &byte, 1);
    //printf("%d\n", byte);
    //fprintf(log, "Hello?%d\n", send);
    //fprintf(log, "Hi?\n");
    //fclose(log);
    //Pray that it makes it to the arduino...
}

int main(int argc, char **argv) {

    int enableNCurses = 1;
    int enableColor = 0;
    int enableArduino = 0; 

    for (int a = 1; a < argc; a++) {
        if (!strcmp(argv[a], "--ard")) {
            if(!sscanf(argv[a+1], "%d", &enableArduino)) {
                fprintf(stderr, "Invalid argument for --ard\nUsage: %s --ard [1][0]\n", argv[0]);
                exit(1);
            }
            a++;
        }
        if (!strcmp(argv[a], "--disp")) {
            if(!sscanf(argv[a+1], "%d", &enableNCurses)) {
                fprintf(stderr, "Invalid argument for --disp\nUsage: %s --ard [1][0]\n", argv[0]);
                exit(1);
            }
            a++;
        }
        if (!strcmp(argv[a], "--color")) {
            if(!sscanf(argv[a+1], "%d", &enableArduino)) {
                fprintf(stderr, "Invalid argument for --color\nUsage: %s --ard [1][0]\n", argv[0]);
                exit(1);
            }
            a++;
        }
    }
    //Ncurses set up
    //if (enableNCurses) {
    initscr();
    noecho();
    int h, w;
    WINDOW *win = newwin(LINES, COLS, 0, 0);
    curs_set(0);
    if (has_colors() == FALSE) {
    endwin();
    printf("Your terminal does not support color\n");
    exit(1);
    }
    if (argc > 1) {
    if (enableColor) {
        init_color(DEFAULT_COLOR, 700, 0, 0);
        start_color();
        init_pair(1, DEFAULT_COLOR, 0);
        attron(COLOR_PAIR(1));
    }
    }
    //}
    



    pa_simple *s;
    pa_sample_spec ss;
    ss.format = PA_SAMPLE_S16NE;
    ss.channels = 1;
    ss.rate = 44100;
    int error;
    // s = pa_simple_new(NULL,           // Use the default server.
    //               "Audio Stream",     // Our application's name.
    //               PA_STREAM_RECORD,
    //               NULL,               // Use the default device.
    //               "TestStream",       // Description of our stream.
    //               &ss,                // Our sample format.
    //               NULL,               // Use default channel map
    //               NULL,               // Use default buffering attributes.
    //               &error              // Ignore error code.
    //               );
    paSimpleRecordOpen(&s, &ss, "Visualizer");
    if (error < 0) {
        //printf("Couldn't open audio server with error code %d\n", error);
        //exit(1);
    }
    

    int bufsize = 512 * 8;
    //u_int16_t buffer[bufsize];
    int16_t *buffer = malloc(sizeof(int16_t) * bufsize);

    long *re = malloc(sizeof(long) * FREQ_SAMP * INTER_SAMP);
    long *im = malloc(sizeof(long) * FREQ_SAMP * INTER_SAMP);
    int *prev = malloc(sizeof(int) * FREQ_SAMP);
    for (int i = 0; i < FREQ_SAMP; i++) prev[i] = 0;

    int fd = 0;
    if (enableArduino) {
        fd = open("/dev/ttyACM0", O_RDWR);
        //FILE *log = fopen("audio.log", "w");
        if (fd <= 0) {
            perror("Failed to open Arduino");
        } 
    }


    while(1) {
        receiveInput(buffer, bufsize, s);
        //processInput(buffer, bufsize, win, re, im);
        processInputfft(buffer, bufsize, win, prev);
        if (enableArduino) sendOutputArduino(fd, 0, prev);
    }
    free(buffer);
    free(re);
    free(im);
    free(prev);
}