#include "fft.h"

#define PI_2 6.28318530718 

/*
 * Digital Fourier Transform, takes in a double data array of size size
 * and fills an array of real and imagniary values with the same size as the 
 * data's size. Calling this function gives approximately the same output as 
 * fft, but is considerably slower ~ O(n^2)
 */
void dft(double *data, double *re, double *im, int size) {
    for (int i = 0; i < size; i++) {
        for (int f = 0; f < size; f++) {
            re[f] += cos(PI_2 * (double) f * i / (double) size) * data[i];
            im[f] -= sin(PI_2 * (double) f * i/ (double) size) * data[i];
        }
    }
}


/*
 * Fast fourier series. Takes in integer data of size size and outputs the frequency
 * spectrum *freq. This output is the squared magnitude of the fft, since it takes more 
 * time to find the real magnitude by square rooting it. To make the process quicker, the
 * maximum frequency is cut off by half its original value. It does this by reducing the number
 * of samples by 2 after the Rearrange function.
 */
void fft(int *data, double *freq, int size) {
    fftRearrange(data, size);

    double re[size];
    double im[size];
    for (int i = 0; i < size; i++) {
        re[i] = (double) data[i];
        im[i] = 0;
    }
    fftRecursive(re, im, size / 2, 1); // This is divided by 2 so that we can see if we can get away with only computing half the fft

    

    // double max = 0;
    // int maxharmonic = 0;
    double freqdoublesquared[size];
    //double dfreqdoublesquared[size];
    for (int i = 0; i < size; i++) {
        freqdoublesquared[i] = re[i]*re[i] + im[i]*im[i];
        //dfreqdoublesquared[i] = dre[i]*dre[i] + dim[i]*dim[i];
        // if (freqdoublesquared[i] > max) {
        //     max = freqdoublesquared[i];
        //     maxharmonic = i;
        // }
        //if (i % 100 == 0 || i <= 100) printf("%d %f\n", i, log(freqdoublesquared[i]));
        //Write the frequency data into the input value freq
        freq[i] = freqdoublesquared[i] * 4; // Multiplied by four, since we cut the fft in half (Line 52)
    }
    
}

/*
 * Takes in a data array of size size and rearranges the elements in 
 * least significant order. For example, let's say the array A has the
 * following inputs at these indices (the index is encoded in binary)
 * A[00] = 5
 * A[01] = 2
 * A[10] = 12
 * A[11] = 7
 * After this function, the Array will look like this
 * Value:       Original Index
 * A[00] = 5    00
 * A[01] = 12   10
 * A[10] = 2    01
 * A[11] = 7    11
 */
void fftRearrange(int *data, int size) {
    int copydata[size];
    int m = log2(size);
    //Copy all data to copydata
    for (int i = 0; i < size; i++) copydata[i] = data[i];

    for (unsigned int index = 0; index < size; index++) {
        unsigned int reverseindex = 0;
        for (int i = 0; i < m; i++) {
            int bit = ((1 << i) & index) >> i;
            reverseindex |= bit << (m - 1 - i);
        }
        data[index] = copydata[reverseindex];
    }
}

/*
 * This function is called recursively by itself and by the function fft()
 */
void fftRecursive(double *re, double *im, int size, int isTopLevel) {
    if (size == 1) return;

    // Even side
    fftRecursive(re, im, size / 2, 0);
    // Odd side
    fftRecursive(&re[size/2], &im[size/2], size/2, 0);

    // Combine the even and odd portions with the butterfly
    // Math explaination here: https://towardsdatascience.com/fast-fourier-transform-937926e591cb
    int a = size / 2;
    if (isTopLevel) a = size / 4;
    for (int i = 0; i < a; i++) {
        double evenre = re[i];
        double evenim = im[i];
        double oddre = re[i + size / 2];
        double oddim = im[i + size / 2];
        double c = cos(PI_2 * (double) i / (double) size);
        double s = sin(PI_2 * (double) i / (double) size);

        re[i] = evenre + oddre * c - oddim * s;
        im[i] = evenim + oddim * c + oddre * s;
        re[i + size/2] = evenre - oddre * c + oddim * s;
        im[i + size/2] = evenim - oddim * c - oddre * s;

    }
}
