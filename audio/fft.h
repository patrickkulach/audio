#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifndef FFT_H
#define FFT_H
// Takes a 2^N size array, and rearranges it into reverse bit order
// The size of the array must be a power of 2, or else trash happens
void fftRearrange(int *data, int size);

// Takes a 2^N size signal rearranged in bit reverse order, and converts it into the 
// frequency domain. 
void fft(int *arrangeddata, double *freq, int size);

void fftRecursive(double *re, double *im, int size, int isTopLevel);

#endif