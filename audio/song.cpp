#include <stdio.h>
#include <stdlib.h>
#include "song.hpp" 

// Fills data with an array of 32 bit floating point little endian PCM at 44100 Hz
// path is the path of the song
int decodeAudio(const char* path, const int sample_rate, float** data, int* size) {
    AVFormatContext * av_format_ctx = avformat_alloc_context();
    if (!av_format_ctx) {
        printf("Couldn't create AVFormatContext\n");
        return -1;
    }

    if(avformat_open_input(&av_format_ctx, path, NULL, NULL) < 0) {
        printf("Could not open Audio file\n");
        return -1;
    } 
    if (avformat_find_stream_info(av_format_ctx, NULL) < 0) {
        printf("Could not find stream info\n");
        return -1;
    }

    int audioStreamIndex = 0; //hardcoded index we're looking at mp3s here
    AVCodecParameters *av_codec_params;
    AVCodec *av_codec;
    av_codec_params = av_format_ctx->streams[audioStreamIndex]->codecpar;
    av_codec = avcodec_find_decoder(av_codec_params->codec_id);

    //Set up codec context for decoder
    AVCodecContext * av_codec_ctx = avcodec_alloc_context3(av_codec);
    if (!av_codec_ctx) {
        printf("Could not create AVCodecContext\n");
        return -1;
    }

    if (avcodec_parameters_to_context(av_codec_ctx, av_codec_params) < 0) {
        printf("Could not load parameters\n");
        return -1;
    }
    if (avcodec_open2(av_codec_ctx, av_codec, NULL) < 0) {
        printf("Could not open Codec\n");
        return -1;
    }
    
    //avcodec_register_all();

    AVPacket av_pkt;
    av_init_packet(&av_pkt);
    AVFrame * av_frame = av_frame_alloc();
    if (!av_frame) {
        printf("Couldn't allocated frame\n");
        return -1;
    }

    // duration is in us, 44100 is sample rate in Hz, and nb_streams is 2 for dual channel and 1 for mono
    // TODO add support for dual channels
    *size = av_format_ctx->duration * 44100 / 1000000;
    //*size = 1000000000;
    *data = (float*) malloc(*size * sizeof(float));
    int samplei = 0;

    while (av_read_frame(av_format_ctx, &av_pkt) >= 0) {
        if (av_pkt.stream_index != 0) {
            continue;
        }
        int response = avcodec_send_packet(av_codec_ctx, &av_pkt);
        if (response < 0) {
            //printf("Failed to send packet to decoder: %s\n", av_err2str(response));
            printf("Failed to send packet to decoder\n");
            return -1;
        }
        response = avcodec_receive_frame(av_codec_ctx, av_frame);
        if (response == AVERROR(EAGAIN) || response == AVERROR_EOF) {
            continue;
        }
        //Write data into data array
        memcpy(&(*data)[samplei], av_frame->extended_data[0], av_frame->nb_samples * sizeof(float));
        samplei+= av_frame->nb_samples;
    }
    //printf("%d, %d\n", *size, samplei);

    avformat_close_input(&av_format_ctx);
    avformat_free_context(av_format_ctx);
    avcodec_free_context(&av_codec_ctx);

    return 0;
}

int convert(void ** data, int size, audioformat in, audioformat out) {
    // Convert to 32 le float by default
    float * fldata;
    switch (in) {
        case F32LE: 
            fldata = (float*) malloc(size * sizeof(float));
            memcpy(fldata, *data, size * sizeof(float));
        break;

        case S16LE:
        fldata = (float*) malloc(size * sizeof(float));
        for (int i = 0; i < size; i++) fldata[i] = ((int16_t*) (*data))[i] / (float) INT16_MAX;
        break; 

        default:
        perror("Audio format not supported yet, please try another one\n");
        return 1;
        break;
    }
    free(*data);
    switch (out) {
        case F32LE:
        *data = malloc(size * sizeof(float));
        memcpy(*data, fldata, size * sizeof(float));
        break;

        case S16LE:
        *data = malloc(size * sizeof(int16_t));
        for (int i = 0; i < size; i++) ((int16_t*) (*data))[i] = (int16_t) (fldata[i] * INT16_MAX);
        break;

        default:
        perror("Audio format not supported yet, please try another one\n");
        return 2;
        break;
    } 
    return 0;
}

int main(int argc, char ** argv) {
    //char path[128] = "../samples/digitallove.mp3";
    //char path[128] = "../samples/mp3ex.mp3";
    char path[128] = "samples/digitallove.mp3";
    float * data;
    int size;
    decodeAudio(path, 44100, &data, &size);
    //FILE * outaud = fopen("outraw", "w");
    //fwrite(data, 1, size*sizeof(float), outaud);
    convert((void**) &data, size, F32LE, S16LE);
    int min = 0;
    int max = 0;
    for (int i = 0; i < size; i++) {
        if (((int16_t*) data)[i] > max) max = ((int16_t*) data)[i];
        if (((int16_t*) data)[i] < min) min = ((int16_t*) data)[i];
    }
    printf("%d %d\n", min, max);
    
    pa_simple * s;
    pa_sample_spec ss;
    paSimpleRecordOpen(&s, &ss, "MP3 Player");
    for (int i = 0; i < size; i++) sendOutput((int16_t*) ((void*) data + i*2), 1, s);
    FILE * f = fopen("rawout", "w");
    fwrite(data, 2, size, f);
    fclose(f);
}