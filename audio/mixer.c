#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <pulse/pulseaudio.h>
#include <pulse/simple.h>
#include <pulse/mainloop.h>
#include <pulse/stream.h>
#include <curses.h>

#define PI 3.141592654359

#define SAMPLE_RATE 44100 //Hz
#define LENGTH 50 //seconds
#define PROC_TIME 1.0f // Time in seconds that it analyses the frequency for
#define EXP_CONST 3.0f // The inputs are multiplied by exp(EXP_CONST * t) when put through the laplace transform
#define MIN_FREQ 100 //Hz
#define MAX_FREQ 5000 //Hz
#define FREQ_SAMP 10 //Amount of frequency samples to be recorded each displayed sample
#define INTER_SAMP 100 //Amount of samples to be displayed


//Adds up all the sin and cos waves from the impulses, but before that it multiplies it by an exponent
//Returns the frequency domain in a double format from MIN_FREQ to MAX_FREQ
double laplaceTransform(double *lt, int *buffer, int index, int bufsize) {
    double re, im = 0;
    double samplePeriod = 1 / (double) SAMPLE_RATE;
    double frequencyStep = (double) (MAX_FREQ - MIN_FREQ) / (double) FREQ_SAMP / (double) INTER_SAMP;
    for (int i = 0; i < FREQ_SAMP * INTER_SAMP; i++) {
        re = 0;
        im = 0;
        double frequency = (double) i * frequencyStep + (double) MIN_FREQ;
        for (int j = 0; j < bufsize; j++) {
            double timedif = (double) (bufsize - j) / (double) bufsize; //Always positive
            double amp = (double) buffer[(index + j) % bufsize];
            re += amp * exp( -EXP_CONST * timedif) * cos(-2*PI * frequency * timedif);
            im += amp * exp( -EXP_CONST * timedif) * cos(-2*PI * frequency * timedif);
        }
        lt[i] = sqrt(re*re + im*im);
    }
}


// Not really a fourier transform, but it accepts a new value, shifts the rest of the frequencies, and then adds the boi
double fastFourierTransform(double *lt, double *ltRe, double *ltIm, int *buffer, int index, int bufsize, int blockSize) {
    double mult = .5; // At the 44100th beat, it will be .012 it's original value
    double samplePeriod = 1 / (double) SAMPLE_RATE;
    double frequencyStep = (double) (MAX_FREQ - MIN_FREQ) / (double) FREQ_SAMP / (double) INTER_SAMP;
    //double multRe = cos(samplePeriod);
    //double multIm = sin(samplePeriod);
    
    //Update real and imaginary values of lt
    for (int j = 0; j < blockSize; j++) {
        int bufindex = (index - j) % bufsize;
        for (int i = 0; i < FREQ_SAMP * INTER_SAMP; i++) {
            double frequency = (double) i * frequencyStep + (double) MIN_FREQ;
            double multRe = cos(frequency * 2 * PI * samplePeriod * j);
            double multIm = sin(frequency * 2 * PI * samplePeriod * j);
            ltRe[i] = mult * (ltRe[i] * multRe - ltIm[i] * multIm) + (double) buffer[bufindex];
            ltIm[i] = mult * (ltRe[i] * multIm + ltIm[i] * multRe);
            lt[i] = sqrt(ltIm[i] * ltIm[i] + ltRe[i] * ltRe[i]);
        }
    //printf("%f \n", lt[2]);
    }
}

int main() {
#if 1
    initscr();
    noecho();
    int h, w;
    WINDOW *win = newwin(LINES, COLS, 0, 0);
#endif

#if 1
    
    //Stolen from the pulseaudio page
    //https://freedesktop.org/software/pulseaudio/doxygen/simple.html
//This is for the syncronous API
    pa_simple *s;
    pa_sample_spec ss;
    ss.format = PA_SAMPLE_S16NE;
    ss.channels = 1;
    ss.rate = 44100;
    int error;
    s = pa_simple_new(NULL,               // Use the default server.
                  "Audio Stream",           // Our application's name.
                  PA_STREAM_RECORD,
                  NULL,               // Use the default device.
                  "TestStream",            // Description of our stream.
                  &ss,                // Our sample format.
                  NULL,               // Use default channel map
                  NULL,               // Use default buffering attributes.
                  &error               // Ignore error code.
                  );
    if (error < 0) {
        printf("Couldn't open audio server with error code %d\n", error);
        exit(1);
    }

#endif
#if 1
    //This is for the asyncronous (good) API

    int bufsize = SAMPLE_RATE * (int) PROC_TIME;
    pa_sample_spec ss;
    ss.format = PA_SAMPLE_S16NE;
    ss.channels = 1;
    ss.rate = 44100;
    pa_stream *s = pa_stream_new(NULL, "Audio Stream", &ss, NULL);
    pa_stream_connect_record(s, NULL, NULL, 0);

    pa_buffer_attr ba;
    ba.maxlength = bufsize;
    ba.fragsize = (uint32_t) - 1;
    pa_stream_set_read_callback(s, readpulse, NULL);
#endif
#if 1
    int refresh = 16; //Refresh rate, 8 hz
    int index = 0; // This will loop around, tell you where t=0 on the buffer array
    int buffer[bufsize]; 
    int per = SAMPLE_RATE / refresh; // This will give a refresh rate of 8 per second
    double *lt = malloc(sizeof(double) * FREQ_SAMP * INTER_SAMP);
    double *ltRe = malloc(sizeof(double) * FREQ_SAMP * INTER_SAMP);
    double *ltIm = malloc(sizeof(double) * FREQ_SAMP * INTER_SAMP);
    while(1) {
        char data[per];
        //pa_simple_read(s, data, per, &error); 

        if (error < 0) {
            //printf("Couldn't read audio input with error code %d\n", error);
            return 1;
        }

        //Write data to buffer
        for (int i = 0; i < per; i++) {
            buffer[index] = data[i];
            index++;
            index = index % bufsize;
        }
        //Laplace Transform it
        //laplaceTransform(lt, buffer, index, bufsize);
        //fastFourierTransform(lt, ltRe, ltIm, buffer, index, bufsize, per);
        
        /*for (int k = 0; k < FREQ_SAMP; k++) {
            printf("%d ", (int) lt[k]);
        }
        printf("\n");*/
        
        for (int x = 0; x < INTER_SAMP; x++) {
            double val = 0;
            for (int i = 0; i < FREQ_SAMP; i++) {
                val += lt[x * FREQ_SAMP + i];
            }
            for (int y = 0; y < 40; y++) {
                wmove(win, y, x);
                if ((int) log(val / FREQ_SAMP) == y) { waddch(win, 'X');} else {waddch(win, ' ');}
            }
        }
        wrefresh(win); 

    }

    pa_simple_free(s);
#endif
}
