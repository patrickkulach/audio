#include <string.h>
#include <glm/glm.hpp>

#include "fft.h"
#include "pulsecontrol.h"
#include "window.h"
#include "shader.h"
#include "camera.h"

#define WIDTH 600
#define HEIGHT 600
#define LED_HEIGHT_LINE -.9f;
#define LED_COUNT 300
#define BAR_COUNT 1000

const float squareVertices[] = {
        0.0f, 0.0f, 0.0f, // First three vertices make a top left triangle
        1.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 0.0f
    };

float ledVertices[LED_COUNT * 6 * 3]; // Each led has two triangles, each with 3 vertices, we're not using indices for this
float barVertices[BAR_COUNT * 6 * 3]; // This is initialized as an array of squares, and does not change in the CPU, but is modified by uniforms in shader

// Loads the global array ledVertices so that it contains the necessary vertices for the given LED_COUNT
void loadVertices() {
    int numVertices = sizeof(squareVertices) / sizeof(float) / 3;

    float x_min = -1.0f;
    float x_max = 1.0f;
    float xsize = (x_max - x_min) / (float) LED_COUNT;
    float y_min = -1.0f;
    float y_max = LED_HEIGHT_LINE;
    float ysize = (y_max - y_min);
    float translated[sizeof(squareVertices) / sizeof(float)];

    for (int i = 0; i < LED_COUNT; i++) {
        memcpy(translated, squareVertices, 3 * numVertices * sizeof(float));
        for (int j = 0; j < numVertices; j++) {
            float t = translated[3*j + 0];
            translated[3*j + 0] = x_min + xsize * (i+1) * t;
            t = translated[3*j+1];
            translated[3*j + 1] = t * ysize + y_min;
        }
        memcpy(&ledVertices[3 * numVertices * i], translated, 3 * numVertices * sizeof(float));
    }

    // for (int i = 0; i < BAR_COUNT; i++) {
    //     memcpy(translated, squareVertices, 3 * numVertices * sizeof(float));
    //     for (int j = 0; j < numVertices; j++) {
    //         float t = translated[3*j + 0];
    //         translated[3*j + 0] = xBarMin + xBarSize * (i+1) * t;
    //         t = translated[3*j+1];
    //         translated[3*j + 1] = t * yBarSize + yBarMin;
    //     }
    // }
}

void drawBars(Shader s, unsigned int VAO, float * level) {
    float xBarMin = -1.0f;
    float xBarMax = 1.0f;
    float xBarSize = (xBarMax - xBarMin) / (float) BAR_COUNT;
    float yBarMin = LED_HEIGHT_LINE;
    float yBarMax = 1.0f;
    float yBarSize = xBarSize;

    for (int i = 0; i < BAR_COUNT; i++) {
        glm::mat4 model(1.0f);
        //float heightmult = level[i] * .01f;
        float heightmult = level[i] * .05f;
        glm::vec3 translateVec(0.0f);
        translateVec.x = xBarMin + i * xBarSize;
        translateVec.y = LED_HEIGHT_LINE;
        translateVec.z = 0.0f;
        model = glm::translate(model, translateVec);
        model = glm::scale(model, glm::vec3(xBarSize, heightmult, 1.0f));
        s.use();
        s.setMat4("model", model); 
        //s.setMat4("view", glm::mat4(1.0f));
        //glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float)WIDTH/(float)HEIGHT, 0.1f, 100.0f);
        //s.setMat4("perspective", proj);
        s.setVec3("ledColor", glm::vec3((float) i / (float) BAR_COUNT,1.0f,0.0f));
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
    }
}

void processInput(int16_t *buffer, int bufsize, float* level, float* previous) {
    int data[bufsize];
    double freq[bufsize];
    for (int i = 0; i < bufsize; i++) data[i] = (int) buffer[i];
    fft(data, freq, bufsize);
    for (int x = 0; x < BAR_COUNT; x++) {
        // Explaination for this line:
        // bufsize is divided by 24 to get rid of higher frequencies that we do not care about
        // The multiplication is done so that we can get an exponential ish frequency chart, and not 
        // a purely linear (harmonic linear not frequency linear)
        int harmonic = bufsize / 24 * ((exp((double) x / (double) BAR_COUNT) - 1) / (M_E - 1));
        
        double lev = 0;
        lev = freq[harmonic] / (double) 100;
        if (lev > .5) {
            lev = log(lev) * 2 - 20;
            lev = pow(lev, 1.1) - 10;
        } else {
            lev = 1;
        }

        if (isnan(lev)) lev = 1;
        if (lev < level[x]) {
            lev = level[x] - ((double) level[x] - lev) * .1;
        }
        int l = floor(lev);

        previous[x] = level[x];
        level[x] = lev;
    }
}

void processInputfft(int16_t *buffer, int bufsize, float* lev, float *previous) {
    //Extra step, convert int16_t to int, should be reorganized if it works
    int data[bufsize];
    double freq[bufsize];
    for (int i = 0; i < bufsize; i++) data[i] = (int) buffer[i];
    fft(data, freq, bufsize);
    
    for (int x = 0; x < BAR_COUNT; x++) {
        int harmonic = bufsize / 24 * ((exp((double) x / (double) BAR_COUNT) - 1) / (2.71828 - 1)); //Replace 24 with 16 for the one you're used to
        double level = 0;
        level = freq[harmonic] / (double) (100);
        if (level > .5) {
            level = log(level) * 2 - 20;
            level = pow(level, 1.1) - 10;
        } else level = 1;
        if (isnan(level)) level = 1;
        if (level < previous[x]) {
            level = previous[x] - ((double) previous[x] - level) * .05;
        }
        if (level <= 1) level = 1.1;
        int l = floor(level);
        lev[x] = level;
        previous[x] = level;

    }

}

int main() {
    // Set up pulse audio
    pa_simple *s;
    pa_sample_spec ss;
    ss.format = PA_SAMPLE_S16NE;
    ss.channels = 1;
    ss.rate = 44100;
    int error;
    //paSimpleRecordOpen(&s, &ss, "Visualizer");
    s = pa_simple_new(NULL,           // Use the default server.
              "Audio Stream",     // Our application's name.                  
              PA_STREAM_RECORD,
              NULL,               // Use the default device.
              "Audio Stream",       // Description of our stream.
              &ss,                // Our sample format.
              NULL,               // Use default channel map
              NULL,               // Use default buffering attributes.
              NULL             // Ignore error code.
              );

    int bufsize = 512 * 8;
    int16_t *buffer = (int16_t*) malloc(bufsize * sizeof(int16_t));
    float level[BAR_COUNT];
    for (int i = 0; i < BAR_COUNT; i++) level[i] = 0.0f;
    float previous[BAR_COUNT];


    // Set up glfw and window
    Window win(WIDTH, HEIGHT);
    win.init();

    Shader ledbar("shaders/ledsim.vs", "shaders/ledsim.fs");
    Shader barShader("shaders/bar.vs", "shaders/ledsim.fs");
    loadVertices();
    unsigned int ledVAO;
    unsigned int ledVBO;
    unsigned int barVAO;
    unsigned int barVBO;

    float ledCols[3 * LED_COUNT];

    glGenVertexArrays(1, &ledVAO);
    glGenBuffers(1, &ledVBO); 
    glBindVertexArray(ledVAO);
    glBindBuffer(GL_ARRAY_BUFFER, ledVBO);
    glBufferData(GL_ARRAY_BUFFER, 6 * LED_COUNT * 3 * sizeof(float), ledVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), NULL);

    glGenVertexArrays(1, &barVAO);
    glGenBuffers(1, &barVBO);
    glBindVertexArray(barVAO);
    glBindBuffer(GL_ARRAY_BUFFER, barVBO);
    glBufferData(GL_ARRAY_BUFFER, 6 * 3 * sizeof(float), squareVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), NULL);

    while(!glfwWindowShouldClose(win.window)) {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        cam::processInput(win.window); // Camera isn't used, but my library has this set up so that escape exits the window

        // Get audio and process it 
        receiveInput(buffer, bufsize, s);
        processInput(buffer, bufsize, level, previous);

        // Draw bottom bar
        ledbar.use();
        glBindVertexArray(ledVAO);
        for (int i = 0; i < LED_COUNT; i++) {
            ledbar.setVec3("ledColor", (float) i / (float) LED_COUNT, 1.0f, 0.0f);
            glDrawArrays(GL_TRIANGLES, 6*i, 6);
        }
        drawBars(barShader, barVAO, level);
        
        glfwSwapBuffers(win.window);
        glfwPollEvents();
    }
    free(buffer);
    pa_simple_free(s);
}