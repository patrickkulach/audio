#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>

#ifndef CAMERA_H
#define CAMERA_H
namespace cam {
//Class methods
void processInput(GLFWwindow* win);
void renderObjects();
void updateCamera();
void mouseCallback(GLFWwindow* window, double xpos, double ypos);
glm::mat4 right3DViewMat(float amp);
glm::mat4 left3DViewMat(float amp);
glm::mat4 viewMat();
void updateDimension(int newwidth, int newheight);
} //End of namespace

#endif