#include "glad.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <iostream>

#include "camera.h"

#ifndef WINDOW_H
#define WINDOW_H

class Window {
    public: 
    Window(int width, int height);
    ~Window();
    void init();

    int width;
    int height;
    GLFWwindow * window = NULL;

};

void framebuffer_size_callback(GLFWwindow* win, int newwidth, int newheight);

#endif 