Command line audio visualizer, using pulseaudio and ncurses. It will read a recording from the default audio input, and display the frequency spectrum on a logarithmic scale.

Depends on Packages
libpulse-dev
libncurses
