#version 330 core

uniform vec3 ledColor;
in vec3 normal;
out vec4 FragColor;

vec3 ray = normalize(vec3(.5, .5, .5));

void main() {
    vec3 col = ledColor * (.5 + .5 * max(dot(normal, ray), 0.0));
    FragColor = vec4(col, 1.0f);

}