#version 330 core
//in vec3 ledcolout;
out vec4 FragColor;

uniform vec3 ledColor;

void main() {
    // FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
    // FragColor = vec4(ledcolout, 1.0f);
    // FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
    FragColor = vec4(ledColor, 1.0f);
}