#version 330 core
layout (location = 0) in vec3 inpos;
layout (location = 1) in vec3 inNormal;
uniform mat4 model;
uniform mat4 view;
uniform mat4 perspective;

out vec3 normal;

void main() {
    gl_Position = perspective * view * model * vec4(inpos, 1.0f);
    normal = vec3(model * vec4(inNormal, 1.0f));
}