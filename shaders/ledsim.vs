#version 330 core
layout (location = 0) in vec3 inpos;
//layout (location = 1) in vec3 ledcol; // RGB of led color 

//out vec3 ledcolout;

void main() {
    // pos = inpos;
    gl_Position = vec4(inpos, 1.0f);
}