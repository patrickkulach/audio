CC = g++
CFLAGS = -g -c -Wall -Iinclude -fpermissive
LIB = -lm -lpulse-simple -lncurses -lavcodec -lavformat -lavutil -lswresample -lstdc++ 
GLLIB = -lglfw -lGL -lX11 -lXi -lXxf86vm -lXinerama -lXcursor -lrt -lassimp -lpthread -ldl

.PHONY: all clean

all: mix decode glmix glmix3d

# Audio based files
build/newmixer.o : audio/newmixer.cpp
	$(CC) $(CFLAGS) audio/newmixer.cpp -o build/newmixer.o 
build/glmixer.o : audio/glmixer.cpp
	$(CC) $(CFLAGS) audio/glmixer.cpp -o build/glmixer.o
build/glmixernew.o : audio/glmixernew.cpp
	$(CC) $(CFLAGS) audio/glmixernew.cpp -o build/glmixernew.o
build/fft.o : audio/fft.*
	$(CC) $(CFLAGS) audio/fft.c -o build/fft.o
build/pulsecontrol.o : audio/pulsecontrol.*
	$(CC) $(CFLAGS) audio/pulsecontrol.c -o build/pulsecontrol.o
build/song.o : audio/song.cpp audio/song.hpp
	$(CC) $(CFLAGS) audio/song.cpp -o build/song.o

# OpenGL Files
build/camera.o : graphics/camera.cpp
	$(CC) $(CFLAGS) graphics/camera.cpp -o build/camera.o
build/shader.o : graphics/shader.cpp
	$(CC) $(CFLAGS) graphics/shader.cpp -o build/shader.o
build/window.o : graphics/window.cpp
	$(CC) $(CFLAGS) graphics/window.cpp -o build/window.o
build/glad.o : graphics/glad.c
	$(CC) $(CFLAGS) graphics/glad.c -o build/glad.o


mix : build/fft.o build/newmixer.o build/pulsecontrol.o
	$(CC) build/fft.o build/newmixer.o build/pulsecontrol.o -o mix $(LIB)

decode : build/song.o build/pulsecontrol.o
	$(CC) build/song.o build/pulsecontrol.o -o decode $(LIB)

glmix : build/fft.o build/pulsecontrol.o build/glmixer.o build/window.o build/camera.o build/shader.o build/glad.o
	$(CC) build/window.o build/camera.o build/shader.o build/glad.o build/fft.o build/pulsecontrol.o build/glmixer.o -o glmix $(LIB) $(GLLIB)

glmix3d : build/fft.o build/pulsecontrol.o build/glmixernew.o build/window.o build/camera.o build/shader.o build/glad.o
	$(CC) build/window.o build/camera.o build/shader.o build/glad.o build/fft.o build/pulsecontrol.o build/glmixernew.o -o glmix3d $(LIB) $(GLLIB)

clean: 
	rm mix glmix glmix3d decode build/*.o
