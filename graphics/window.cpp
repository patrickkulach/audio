#include "window.h"

Window::Window(int width, int height) : width(width), height(height) {}

Window::~Window() {
    if (window != NULL) {
        //glfwDestroyWindow(window);
    }
}

void Window::init() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //Current version is 3.0
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    window = glfwCreateWindow(width, height, "Visualizer", NULL, NULL);

    
    if (window == NULL) {
        std::cout << "Failed to initialize GLFW" << std::endl;
    }
    glfwMakeContextCurrent(window); //Most important instruction that I always forget
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD" << std::endl;
    }

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); //Disables the cursor so you dont see it


    glViewport(0, 0, width, height); //Initializes the viewport to this context, split into two during render time for 3D fun!

    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback); //Creates the callback function
    glfwSetCursorPosCallback(window, cam::mouseCallback); //Set mouse callback function
    glEnable(GL_DEPTH_TEST);
}

void framebuffer_size_callback(GLFWwindow * win, int newwidth, int newheight) {
    glViewport(0, 0, newwidth, newheight);
    cam::updateDimension(newwidth, newheight);
    long winl = (long) win; //This line just prevents the warning "win is not used" I don't know if I can safely get rid of win though
    winl += 1;
}